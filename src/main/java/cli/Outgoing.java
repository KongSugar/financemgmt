package cli;

import payload.Payment;

/**
 * Created by Niko on 13.03.2015.
 */
public class Outgoing {

    public static void show() {
//        for (int i = 0; i < 160; i++) {
//            System.out.println();
//        }

        System.out.println("CREATING NEW OUTGOING PAYMENT");
        Payment payment = new payload.Outgoing();
        System.out.print("VALUE: ");
        payment.setValue(Control.getBigDecimal());
        System.out.println();
        System.out.print("NAME: ");
        payment.setObject(Control.getString());
        payment.pay();

        System.out.println(payment.getObject() + " " + payment.getValue());
    }
}
