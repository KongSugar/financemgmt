package cli;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Created by Niko on 13.03.2015.
 */
public class Control {
    public static int getInt() {
        int i = 0;

        //System.out.print("Enter a number: ");
        Scanner in = new Scanner(System.in);

        try {
            i = in.nextInt();
        } catch (Exception e) {
            System.out.println("False Input!");
            i = getInt();
        }
        return i;
    }

    public static String getString() {
        String s = null;
        Scanner in = new Scanner(System.in);

        try {
            s = in.nextLine();
        } catch (Exception e) {
            System.out.println("False Input!");
            s = getString();
        }

        return s;
    }

    public static BigDecimal getBigDecimal() {
        BigDecimal bd;
        Scanner in = new Scanner(System.in);

        try {
            bd = in.nextBigDecimal();
        } catch (Exception e) {
            System.out.println("False Input!");
            bd = getBigDecimal();
        }
        return bd;
    }
}
