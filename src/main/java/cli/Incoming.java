package cli;

import payload.Payment;

/**
 * Created by Niko on 13.03.2015.
 */
public class Incoming {

    public static void show() {
//        for (int i = 0; i < 160; i++) {
//            System.out.println();
//        }
        System.out.println("CREATE NEW INCOMING PAYMENT");
        Payment payment = new payload.Incoming();
        System.out.print("VALUE: ");
        payment.setValue(Control.getBigDecimal());
        System.out.println();
        System.out.print("NAME: ");
        payment.setObject(Control.getString());
        payment.pay();

        System.out.println(payment.getObject() + " " + payment.getValue());


    }

}
