package cli;

/**
 * Created by Niko on 13.03.2015.
 */
public class CLI {

    public static void main(String[] args) {
        showTitle();

        while (true) {
            Menu.start();
        }
    }

    private static void showTitle() {
        for (int i = 0; i < 80; i++) {
            System.out.print('#');
        }
        System.out.println();
        System.out.println("Finance MgMt");
        System.out.println();
    }

}
