package cli;

/**
 * Created by Niko on 13.03.2015.
 */
public class Menu {
    private static void menuInput() {
        int choosen = Control.getInt();
        switch (choosen) {
            case 1:
                Incoming.show();
                break;
            case 2:
                Outgoing.show();

                break;
            case 5:
                Status.show();
                break;
            case 0:
                System.out.println("Closing program...");
                System.exit(0);
            default:
                System.out.println("Wrong Number");
                menuInput();

        }
    }

    public static void start() {
        System.out.println("Choose: ");
        System.out.println("1) Make an Incoming Payment");
        System.out.println("2) Make an Outgoing Payment");
        System.out.println("5) Show current status");
        System.out.println("0) Close");
        System.out.println();
        menuInput();
    }
}
