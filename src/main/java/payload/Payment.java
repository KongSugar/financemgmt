package payload;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Niko on 13.03.2015.
 */
public abstract class Payment {
    private boolean done = false;
    private BigDecimal value;
    private String person;
    private String object;
    private Date date;

    public abstract void pay(BigDecimal value);

    public void pay(){
        pay(getValue());
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String toString() {
        String o = object+" "+value+"\n";
        return o;
    }
}
