package payload;

import data.Payments;
import treasure.Treasure;

import java.math.BigDecimal;

/**
 * Created by Niko on 13.03.2015.
 */
public class Incoming extends Payment {
    public Incoming() {
        super();
        Payments.in.add(this);

    }

    @Override
    public void pay(BigDecimal value) {
        if (!isDone()) {
            Treasure t = Treasure.getInstance();
            BigDecimal balance = t.getBalance();
            balance = balance.add(value);
            t.setBalance(balance);
            setDone(true);
        }
    }
}
