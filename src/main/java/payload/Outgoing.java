package payload;

import data.Payments;
import treasure.Treasure;

import java.math.BigDecimal;

/**
 * Created by Niko on 13.03.2015.
 */
public class Outgoing extends Payment {
    public Outgoing() {
        super();
        Payments.out.add(this);
    }

    @Override
    public void pay(BigDecimal value) {
        if (!isDone()) {
            Treasure t = Treasure.getInstance();
            BigDecimal balance = t.getBalance();
            balance = balance.subtract(value);
            t.setBalance(balance);
            setDone(true);
        }
    }
}
