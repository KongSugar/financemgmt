package treasure;

import java.math.BigDecimal;

/**
 * Created by Niko on 13.03.2015.
 */
public class Treasure {

    private static Treasure instance = null;
    private BigDecimal balance;

    protected Treasure() {
        balance = new BigDecimal(0);
    }

    public static Treasure getInstance() {
        if (instance == null) {
            instance = new Treasure();
        }
        return instance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
