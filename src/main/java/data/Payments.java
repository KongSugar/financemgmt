package data;

import payload.Incoming;
import payload.Outgoing;

import java.util.ArrayList;

/**
 * Created by Niko on 13.03.2015.
 */
public class Payments {
    public static ArrayList<Outgoing> out = new ArrayList<>();
    public static ArrayList<Incoming> in = new ArrayList<>();
    private static Payments instance = null;


    protected Payments() {
    }

    public static Payments getInstance() {
        if (instance == null) {
            instance = new Payments();
        }
        return instance;
    }
}
